import { LitElement, html } from 'lit-element';

// Finalmente no utilizo este webcomponent. Se hace de una manera distinta. //

class PersonaFiltroEdad extends LitElement {

    static get properties() {
        return {
            years: {type: Number}
        };
    }

    constructor() {
        super();
    }

    render(){
        return html`
        <div>
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            Filtro selección antiguedad en la empresa en años
            0 <input type="range" value="100" min="0" max="100" autocomplete="off" step="1" list="tickmarks" @input="${this.updateYears}"> 100
            <datalist id="tickmarks">
                <option value="0">
                <option value="10">
                <option value="20">
                <option value="30">
                <option value="40">
                <option value="50">
                <option value="60">
                <option value="70">
                <option value="80">
                <option value="90">
                <option value="100">
            </datalist>
            <span>Años: ${this.years}</span>
        </div>
        `;
    }

    updateYears(e) {
        console.log("updated en persona-filtro-edad");
        // Muestra el valor de los años que se está seleccionando                                             //
        this.years = e.target.value;
    }

    updated(changedProperties) {
        if (changedProperties.has("years")) {
            // Ha cambiado la propiedad years, por la que se va a filtrar los años en la empresa              //
            this.shadowRoot.querySelector("span").value=this.years;
            this.dispatchEvent(new CustomEvent("updated-range",
                {
                   detail: {
                        years: this.years
                    }
                }
            ));
        }
    }

}

customElements.define("persona-filtro-edad", PersonaFiltroEdad);