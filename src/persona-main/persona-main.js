import { LitElement, html } from 'lit-element';
import "../persona-ficha-listado/persona-ficha-listado.js";
import "../persona-form/persona-form.js";
import "../persona-main-dm/persona-main-dm.js";

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
            maxYearsInCompanyFilter: {type: Number}
        };
    }

    constructor() {
        super();

        this.people = [];
        this.showPersonForm = false;
        this.maxYearsInCompanyFilter = 0;
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.filter(
                        person => person.yearsInCompany <= this.maxYearsInCompanyFilter
                    ).map(
                        person => html`<persona-ficha-listado 
                            profile="${person.profile}"
                            fname="${person.name}"
                            yearsInCompany="${person.yearsInCompany}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}"></persona-ficha-listado>`
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form @persona-form-close="${this.personFormClose}"
                <persona-form @persona-form-store="${this.personFormStore}"
                class="d-none" border rounded border-primary id="personForm"></persona-form>
            </div>
            <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>
        `;
    }

    peopleDataUpdated(e) {
        console.log("peopleDataUpdated");

        this.people = e.detail.people;
    }

    updated(changedProperties) {
        console.log("updated");

        // Control de si se muestra la pantalla de formulario o la del listado de personas //
        if(changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            // para que se muestre el formulario //
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        // Control de los cambios para las estadísticas - Número de personas del array //
        if(changedProperties.has("people")){
            // Se detecta cuando cambia el array entero, no solo uno de sus elementos //
            console.log("Ha cambiado el valor de la propiedad people en persona-main");
            
            // Se abre evento para enviar los datos del array a persona-app, desde donde se capturará el evento. // 
            this.dispatchEvent(new CustomEvent("updated-people",{
                detail: {
                    people: this.people
                }
            }));
        }

        // Cambio en el range de años de antiguedad en la empresa                                                //
        if(changedProperties.has("maxYearsInCompanyFilter")){
            console.log("Ha cambiado el valor de la propiedad maxYearsInCompanyFilter");
            console.log("Se van a mostrar las personas cuya antiguedad máxima sea menor a " + this.maxYearsInCompanyFilter + " años.");
        }

    }
    
    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando el formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    }

    // Función Manejadora  //
    personFormClose() {
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de persona");

        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");

        console.log("La propiedad name de person vale " + e.detail.person.name);
        console.log("La propiedad profile de person vale " + e.detail.person.profile);
        console.log("La propiedad yearsInCompany de person vale " + e.detail.person.yearsInCompany);

        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona de nombre" + e.detail.person.name);
            
            //let indexOfPerson = 
            //    this.people.findIndex(
            //        // actualiza la persona //
            //        person => person.name === e.detail.person.name
            //    );
            //if (indexOfPerson >= 0) {
            //    console.log("Persona encontrada");
            //    this.people[indexOfPerson] = e.detail.person;
            //}
            // REFACTORIZADO para el contador:
            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person
            );

        } else {
            console.log("Se va a almacenar una persona nueva");
            // guarda la nueva persona //
            // this.people.push(e.detail.person);
            // REFACTORIZADO para el contador:
            this.people = [...this.people, e.detail.person];
        }

        console.log("Persona almacenada");

        this.showPersonForm = false;
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log(e);
        console.log("Se va a borrar la persona de nombre: " + e.detail.name);
        // borra la persona //
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e) {
        console.log("infoPerson");
        console.log("Se ha pedido más información de la persona " + e.detail.name);

        // chosen person es un array de un elemento
        // selección de la info de la persona
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        // se crea un objeto y se le puebla con info.
        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;

        // Se envian los datos al formulario a través del shadowRoot
        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }

}

customElements.define("persona-main", PersonaMain);