import { LitElement, html } from 'lit-element';

class PersonaStats extends LitElement {

    static get properties() {
        return {
            people : {type: Array}
        };
    }

    constructor() {
        super();

        this.people = [];
    }

    // Como no va a mostrar nada, solo es para hacer el cálculo de personas, en render no debe pintar nada //
    // La info la va a mostrar en sideBar, tal y como hemos pensado.                                       //
    // Por lo que se elimina el render, aunque también se puede dejar sin poner nada dentro del render.    //
    //
    // render(){
    //    return html`
    //        Persona Stats
    //        
    //    `;
    // }

    // El flujo de ejecución lo inicia el momento en el que se produce un cambio en people en persona main //
    
    // Detectamos el cambio en la propiedad que se ha producido en persona-main
    updated(changedProperties) {
        console.log("updated en persona-stats");
        console.log(changedProperties);
        
        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la persona people en persona stats");

            // Llamada para el cálculo de estadísticas                                                     //
            let peopleStats = this.gatherPeopleArrayInfo(this.people);

            // Se le pasa un evento a persona-sidebar, pero para llegar a personasidebar hay que pasar     //
            // por persona-app                                                                             //
            this.dispatchEvent(new CustomEvent("updated-people-stats", {
                detail: {
                    peopleStats: peopleStats
                }
            }));
        }
    }

    gatherPeopleArrayInfo(people) {
        console.log("gatherPeopleArrayInfo");
        
        // Objeto que nos inventamos al que le creamos la propiedad numberOfPeople                         //
        // Aquí se calculará el número de elementos de array.                                              //
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        
        // Se recoje la info del máximo para el botón range
        let maxYearsInCompany = 0;
        people.forEach(
            person => {
                if (person.yearsInCompany > maxYearsInCompany) {
                    maxYearsInCompany = person.yearsInCompany
                }
            }
        )
        console.log("maxYearsInCompany es " + maxYearsInCompany);
        peopleStats.maxYearsInCompany = maxYearsInCompany;

        return peopleStats;
    }
}
customElements.define("persona-stats", PersonaStats);