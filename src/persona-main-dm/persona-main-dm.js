import { LitElement, html } from 'lit-element';

class PersonaMainDm extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();
     
        this.people = [
            {
                name: "Steve Rogers",
                yearsInCompany: 85,
                photo: {
                    src: "./img/Steve.jpg",
                    alt: "Steve Rogers"
                },
                profile: "Capitan America"
            }, {
                name: "Tony Stark",
                yearsInCompany: 5,
                photo: {
                    src: "./img/Tony.jpg",
                    alt: "Tony Stark"
                },
                profile: "IronMan"
            }, {
                name: "Peter Parker",
                yearsInCompany: 1,
                photo: {
                    src: "./img/Peter.jpg",
                    alt: "Peter Parker"
                },
                profile: "SpiderMan el mejor amigo y vecino de la mejor ciudad del mundo mundial"
            }, {
                name: "Bruce Banner",
                yearsInCompany: 4,
                photo: {
                    src: "./img/Bruce.jpg",
                    alt: "Bruce Banner"
                },
                profile: "Hulk"
            },  {
                name: "Natasha Romanov",
                yearsInCompany: 8,
                photo: {
                    src: "./img/Natasha.jpg",
                    alt: "Natasha Romanov"
                },
                profile: "Viuda Negra"
            },  {
                name: "Clint Burton",
                yearsInCompany: 8,
                photo: {
                    src: "./img/Clint.jpg",
                    alt: "Clint Burton"
                },
                profile: "Ojo de Halcon"
            }
        ]
    }

    render(){
        return html`
            
        `;
    }
    
    updated(changedProperties) {
        console.log("updated");

        if (changedProperties.has("people")) {
            console.log ("Ha cambiado la propiedad de people en persona-main-dm");

            this.dispatchEvent(new CustomEvent("people-data-updated", 
                {
                    detail: {
                        people: this.people
                    }
                }           
            ));
        }
    }
}

customElements.define("persona-main-dm", PersonaMainDm);