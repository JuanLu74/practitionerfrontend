import { LitElement, html } from 'lit-element';
import "../persona-filtro-edad/persona-filtro-edad.js";

class PersonaSidebar extends LitElement {

    static get properties() {
        return{       
            peopleStats: {type: Object}

        };
    }

    constructor() {
        super();

        this.peopleStats = {};
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <aside>
                <section>
                    <div>
                        Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
                    </div>
                    <div class="mt-5">
                        <button @click=${this.newPerson} class="w-100 btn bg-success" style="font-size: 50px"><strong>+</strong>
                    </div>
                    <div>
                        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
                        <br>
                        Filtro selección antiguedad en la empresa (años):
                        <input type="range" min="0" 
                            max="${this.peopleStats.maxYearsInCompany}" 
                            step="1" 
                            list="tickmarks" 
                            value="${this.peopleStats.maxYearsInCompany}"
                            @input="${this.updateMaxYearsInCompanyFilter}">
                        <datalist id="tickmarks">
                            <option value="0">
                            <option value="10">
                            <option value="20">
                            <option value="30">
                            <option value="40">
                            <option value="50">
                            <option value="60">
                            <option value="70">
                            <option value="80">
                            <option value="90">
                            <option value="100">
                        </datalist>
                    </div>
                </section>
            </aside>
        `;
    }

    updateMaxYearsInCompanyFilter(e) {
        console.log("updateMaxYearsInCompanyFilter");
        console.log("El range vale " + e.target.value);

        this.dispatchEvent(
            new CustomEvent(
                "updated-max-years-filter",
                {
                    detail: {
                        maxYearsInCompany: e.target.value
                    }
                }
            )
        );
    }

    newPerson(e) {
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear una nueva persona");

        this.dispatchEvent(new CustomEvent("new-person", {}));
    }

}

customElements.define("persona-sidebar", PersonaSidebar);